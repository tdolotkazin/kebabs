import SpriteKit

class MainMenu: SKNode, ButtonDelegate {
	

	var parentScene: GameScene!
	var gameController: GameController!
	
	private var playButton: Button!
	private let buildTime = Constants.UIbuildTime
	private var logo: SKSpriteNode!
	private var bestScore: SKLabelNode!
	private var bestScoreNumber:SKLabelNode!
	
	init(scene: GameScene) {
		super.init()
		parentScene = scene
		self.gameController = parentScene.gameController
		self.zPosition = 100
		createLogo()
		createPlayButton()
		createBestScoreLabel()
		createBestScoreNumber()
		showChildren()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func createLogo() {
		let logoTexture = SKTexture(imageNamed: "Logo")
		logoTexture.filteringMode = .nearest
		logo = SKSpriteNode(texture: logoTexture)
		logo.createShadow()
		logo.position = Layout.MainMenu.logo
		logo.position.x -= 500
		addChild(logo)
		
	}
	
	func createPlayButton() {
		let playButtonTexture = SKTexture(imageNamed: "PlayLarge")
		playButtonTexture.filteringMode = .nearest
		playButton = Button(texture: playButtonTexture)
		playButton.position = Layout.MainMenu.playButton
		playButton.position.x += 500
		playButton.createShadow()
		let playLabel = SKLabelNode(text: "Play")
		playLabel.makePixelFont()
		playLabel.fontColor = #colorLiteral(red: 0.7526851296, green: 0.132222265, blue: 0.08112163097, alpha: 1)
		playLabel.fontSize = 24
		playLabel.zPosition = 1
		playButton.addChild(playLabel)
		playButton.delegate = self
		addChild(playButton)
		
	}
	
	func createBestScoreLabel() {
		bestScore = SKLabelNode(text: "Best score")
		bestScore.fontName = "Press Start 2P"
		bestScore.fontSize = 20
		bestScore.fontColor = .white
		bestScore.position = Layout.MainMenu.bestScore
		bestScore.position.x -= 500
		addChild(bestScore)
		
	}
	
	func createBestScoreNumber() {
		bestScoreNumber = SKLabelNode(text: "\(gameController.bestScore)")
		bestScoreNumber.makePixelFont()
		bestScoreNumber.position = Layout.MainMenu.bestScoreNumber
		bestScoreNumber.position.x += 500
		addChild(bestScoreNumber)
		
	}
	
	
	
	func showChildren() {
		logo.run(SKAction.move(to: Layout.MainMenu.logo, duration: buildTime))
		playButton.run(SKAction.move(to: Layout.MainMenu.playButton, duration: buildTime))
		bestScore.run(SKAction.move(to: Layout.MainMenu.bestScore, duration: buildTime))
		bestScoreNumber.run(SKAction.move(to: Layout.MainMenu.bestScoreNumber, duration: buildTime))
		parentScene.settingsButtons.showAll(during: .MainMenu)
	}
	
	func hideChildren() {
		logo.run(SKAction.moveBy(x: -500, y: 0, duration: buildTime))
		playButton.run(SKAction.moveBy(x: 500, y: 0, duration: buildTime))
		bestScore.run(SKAction.moveBy(x: -500, y: 0, duration: buildTime))
		bestScoreNumber.run(SKAction.moveBy(x: 500, y: 0, duration: buildTime))
		parentScene.settingsButtons.hideAll()
	}
	
	func buttonPressed(button: Button) {
		switch button {
			case playButton:
				self.run(SKAction.sequence([
					SKAction.run(hideChildren),
					SKAction.wait(forDuration: buildTime),
					SKAction.run {
						self.removeFromParent()
					},
					SKAction.run {
						self.gameController.state = .Play
						self.parentScene.restart()
					}]))
			default: fatalError("Unknown button!")
		}
	}
}
