import SpriteKit
import AVFoundation

class Tutorial: SKNode, ButtonDelegate {
	
	private var closeButton: Button!
	
	override init() {
		super.init()
		self.zPosition = 150
	}

	func showTutorial() {
		alpha = 0
		
		guard let url = Bundle.main.url(forResource: "tutorial", withExtension: "mp4") else {
			fatalError("No video found!")
		}
		let player = AVPlayer(url: url)
		player.actionAtItemEnd = .none
		let video = SKVideoNode(avPlayer: player)
		NotificationCenter.default.addObserver(self, selector: #selector(didReachEnd(notification:)), name: .AVPlayerItemDidPlayToEndTime, object: player.currentItem)
		let tutorialFrame = SKSpriteNode(imageNamed: "TutorialFrame")
		video.size = CGSize(width: 307, height: 545)
		video.position = CGPoint(x: frame.midX, y: frame.midY)
		let cropNode = SKCropNode()
		cropNode.maskNode = SKSpriteNode(color: .black, size: CGSize(width: 253, height: 545))
		cropNode.addChild(video)
		
		addChild(cropNode)
		video.play()
		closeButton = Button(color: .clear, size: CGSize(width: 40, height: 40))
		closeButton.position = CGPoint(x: 117, y: 264)
		closeButton.delegate = self
		addChild(tutorialFrame)
		addChild(closeButton)
		run(.fadeIn(withDuration: 1))
	}
	
	@objc func didReachEnd(notification: Notification) {
		if let item = notification.object as? AVPlayerItem {
			item.seek(to: .zero, completionHandler: nil)
		}
	}
	
	func buttonPressed(button: Button) {
		if let parentScene = parent as? GameScene {
			parentScene.audioPlayer.playEffect(of: .ButtonPressed)
			self.removeAllChildren()
			NotificationCenter.default.removeObserver(self)
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
