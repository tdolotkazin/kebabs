import SpriteKit

protocol ShareDelegate: AnyObject {
	func showShareView(shareImage: UIImage, shareText: String)
}

class ScoreBoard: SKNode, ButtonDelegate {
	
	var gameController: GameController!
	weak var delegate: ShareDelegate?
	private var scoreLabel: SKLabelNode!
	private var bestScoreLabel: SKLabelNode!
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		for child in children {
			if let button = child as? Button {
				button.delegate = self
			}
		}
		scoreLabel = (childNode(withName: "scoreLabel") as! SKLabelNode)
		bestScoreLabel = (childNode(withName: "bestScoreLabel") as! SKLabelNode)
		self.isHidden = true
	}
	
	func updateLabels() {
		scoreLabel?.text = String(gameController.score)
		bestScoreLabel?.text = String(gameController.bestScore)
	}
	
	func gameOver() {
		updateLabels()
		self.isHidden = false
	}
	
	func buttonPressed(button: Button) {
		switch button.name {
			case "replay":
				self.isHidden = true
				let scene = self.scene as! GameScene
				scene.restart()
			case "shareButton":
				let shareBoardScene = SKScene(fileNamed: "ScoreBoardScene")
				let shareBoard = shareBoardScene?.childNode(withName: "//scoreBoard") as! ScoreBoard
				for child in shareBoard.children {
					if child is Button {
						child.removeFromParent()
					}
				}
				shareBoard.scoreLabel.text = String(gameController.score)
				shareBoard.bestScoreLabel.text = String(gameController.bestScore)
				shareBoard.isHidden = false
				let texture = self.scene?.view?.texture(from: shareBoard)
				let image = UIImage(cgImage: (texture?.cgImage())!)
				delegate?.showShareView(shareImage: image, shareText: "I've got \(gameController.score) scores in Kebab Truck!")
			default:
				fatalError("Unknown button on the game over screen!")
		}
	}
}
