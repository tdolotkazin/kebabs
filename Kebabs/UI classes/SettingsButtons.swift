import SpriteKit

class SettingsButtons: SKNode, ButtonDelegate {
	
	var audioPlayer: AudioPlayer!
	var tutorial: Tutorial!
	
	private var audioButton: Button!
	private var musicButton: Button!
	private var tutorialButton: Button!
	private var buildTime = Constants.UIbuildTime

	func createButtons() {
		createMusicButton()
		createAudioButton()
		createTutorialButton()
	}
	
	func createMusicButton() {
		musicButton = Button()
		musicButton.stateIsOn = audioPlayer.musicIsEnabled
		musicButton.onTexture = SKTexture(imageNamed: MusicButtonTexture.on.rawValue)
		musicButton.offTexture = SKTexture(imageNamed: MusicButtonTexture.off.rawValue)
		musicButton.updateState()
		musicButton.delegate = self
		musicButton.position = Layout.MainMenu.musicButton
		musicButton.position.x -= 500
		addChild(musicButton)
	}
	
	func createAudioButton() {
		audioButton = Button()
		audioButton.stateIsOn = audioPlayer.effectsAreEnabled
		audioButton.onTexture = SKTexture(imageNamed: AudioButtonTexture.on.rawValue)
		audioButton.offTexture = SKTexture(imageNamed: AudioButtonTexture.off.rawValue)
		audioButton.updateState()
		audioButton.delegate = self
		audioButton.position = Layout.MainMenu.audioButton
		audioButton.position.x -= 500
		addChild(audioButton)
	}
	
	func createTutorialButton() {
		tutorialButton = Button(texture: SKTexture(imageNamed: "Question mark"))
		tutorialButton.delegate = self
		tutorialButton.position = Layout.MainMenu.tutorialButton
		tutorialButton.position.x -= 500
		addChild(tutorialButton)
	}
	
	func showAll(during gameState: GameState) {
		let audioButtonPosition: CGPoint
		let musicButtonPosition: CGPoint
		let tutorialButtonPosition: CGPoint

		switch gameState {
			case .MainMenu, .GameOver:
				audioButtonPosition = Layout.MainMenu.audioButton
				musicButtonPosition = Layout.MainMenu.musicButton
				tutorialButtonPosition = Layout.MainMenu.tutorialButton
			case .Pause, .Play:
				audioButtonPosition = Layout.SettingsButtons.audioButton
				musicButtonPosition = Layout.SettingsButtons.musicButton
				tutorialButtonPosition = Layout.SettingsButtons.tutorialButton
		}
		
		audioButton.position.y = audioButtonPosition.y
		musicButton.position.y = musicButtonPosition.y
		tutorialButton.position.y = tutorialButtonPosition.y
				
		audioButton.run(SKAction.move(to: audioButtonPosition, duration: buildTime))
		musicButton.run(SKAction.move(to: musicButtonPosition, duration: buildTime))
		tutorialButton.run(SKAction.move(to: tutorialButtonPosition, duration: buildTime))
	}
	
	func hideAll() {
		audioButton.run(SKAction.moveBy(x: -500, y: 0, duration: buildTime))
		musicButton.run(SKAction.moveBy(x: -500, y: 0, duration: buildTime))
		tutorialButton.run(SKAction.moveBy(x: -500, y: 0, duration: buildTime))
	}
		
	func buttonPressed(button: Button) {
		switch button {
			case musicButton:
				audioPlayer.musicIsEnabled.toggle()
				musicButton.stateIsOn?.toggle()
				musicButton.updateState()
			case audioButton:
				audioPlayer.effectsAreEnabled.toggle()
				audioButton.stateIsOn?.toggle()
				audioButton.updateState()
				audioPlayer.playEffect(of: .ButtonPressed)
			case tutorialButton:
				tutorial.showTutorial()
				audioPlayer.playEffect(of: .ButtonPressed)
			default:
				fatalError("Unknown settings button")
		}
	}
}
