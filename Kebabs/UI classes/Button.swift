import SpriteKit

protocol ButtonDelegate: AnyObject {
	func buttonPressed(button: Button)
}

class Button: SKSpriteNode {
	weak var delegate: ButtonDelegate?
	private var isPressed: Bool = false
	var isEnabled: Bool = true {
		didSet {
			if !isEnabled {
				self.texture = SKTexture(imageNamed: "emptyButtonDisabled")
			} else {
				self.texture = SKTexture(imageNamed: "emptyButton")
			}
		}
	}
	var stateIsOn: Bool?
	var onTexture: SKTexture?
	var offTexture: SKTexture?
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		isUserInteractionEnabled = true
		scaleIfIpad()
	}
	
	override init(texture: SKTexture?, color: UIColor, size: CGSize) {
		super.init(texture: texture, color: color, size: size)
		isUserInteractionEnabled = true
		scaleIfIpad()
	}
	
	func scaleIfIpad() {
		if name == "shareButton" || name == "replay" {
			return
		}
		if UIDevice.current.userInterfaceIdiom == .pad {
			size.width *= 1.2
			size.height *= 1.2
		}
	}
	
	func updateState() {
		if let state = stateIsOn {
			if state {
				texture = onTexture
				size = texture!.size()
				
			} else {
				texture = offTexture
				size = texture!.size()
			}
			scaleIfIpad()
		}
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		if isEnabled {
			isPressed = true
			self.setScale(0.7)
		}
	}
	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		if let touch = touches.first {
			let location = touch.location(in: parent!)
			if !frame.contains(location) {
				isPressed = false
				self.setScale(1)
			}
		}
	}
	
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
		if !isEnabled {
			return
		} else {
			if isPressed {
				self.setScale(1)
				delegate?.buttonPressed(button: self)
			}
			isPressed = false }
	}
}
