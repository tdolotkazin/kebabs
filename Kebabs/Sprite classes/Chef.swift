import SpriteKit

class Chef: SKSpriteNode {
	
	private var walkingTextures = [SKTexture]()
	private var cookingTextures = [SKTexture]()
	
	override init(texture: SKTexture?, color: UIColor, size: CGSize) {
		super.init(texture: texture, color: color, size: size)
	}
	
	convenience init() {
		self.init(texture: SKTexture(imageNamed: "Cook0"), color: .red, size: CGSize(width: 40, height: 40))
		texture?.filteringMode = .nearest
		self.zRotation = .pi
		self.zPosition = 2
		let walkingAtlas = SKTextureAtlas(named: "ChefWalking")
		for n in 0...7 {
			walkingTextures.append(walkingAtlas.textureNamed("Cook\(n)"))
			walkingTextures[n].filteringMode = .nearest
		}
		let cookingAtlas = SKTextureAtlas(named: "ChefCooking")
		for n in 0...5 {
			cookingTextures.append(cookingAtlas.textureNamed("Cooking\(n)"))
			cookingTextures[n].filteringMode = .nearest
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func stopAnimation() -> SKAction {
		SKAction.run {
			self.removeAction(forKey: "animate")
		}
	}
	
	func moveTo(_ destination: CGPoint, time: TimeInterval) -> SKAction {
		let animate = SKAction.run {
			self.run(SKAction.repeatForever(
				SKAction.animate(with: self.walkingTextures,
								 timePerFrame: 0.0625,
								 resize: false,
								 restore: true)),
					 withKey: "animate")
		}
		let rotateToNextDestination = SKAction.run {
			let dx = self.position.x - destination.x
			let dy = self.position.y - destination.y
			let angle = -atan2(dx, dy)
			self.run(SKAction.rotate(toAngle: angle, duration: time/5, shortestUnitArc: true))
		}
		let move = SKAction.move(to: destination, duration: 4 * time / 5)
		var finalAngle: CGFloat
		switch destination {
			case Constants.cookingPosition:
				finalAngle = -.pi/2
			case Constants.grillPosition:
				finalAngle = 0
			case Constants.chefPostition:
				finalAngle = .pi
			default: finalAngle = 0
		}
		let rotateToPosition = SKAction.rotate(toAngle: finalAngle, duration: 0, shortestUnitArc: true)
		let rotateMoveAndStop = SKAction.sequence([rotateToNextDestination, move, stopAnimation(), rotateToPosition])
		return SKAction.group([animate, rotateMoveAndStop])
	}
	
	func cookAnimation(time: TimeInterval) -> SKAction {
		let animate = SKAction.run {
			self.run(SKAction.repeatForever(
				SKAction.animate(with: self.cookingTextures,
								 timePerFrame: 0.0625,
								 resize: false,
								 restore: true)),
					 withKey: "animate")
		}
		
		let waitAndStop = SKAction.sequence([SKAction.wait(forDuration: time), stopAnimation()])
		return SKAction.group([animate, waitAndStop])
	}
	
	func cook(for time: TimeInterval) {
		run(SKAction.sequence([
				moveTo(Constants.cookingPosition, time: time / 5),
				cookAnimation(time: time / 5),
				moveTo(Constants.grillPosition, time: time / 5),
				cookAnimation(time: time / 5),
				moveTo(Constants.chefPostition, time: time / 5)]))
	}
}
