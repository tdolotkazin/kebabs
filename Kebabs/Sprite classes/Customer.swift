import SpriteKit

class Customer: SKNode {
	
	enum State {
		case walking
		case waitingInTheLine
		case waitingForTheFood
		case leaving
	}
	var groupHeight: CGFloat {
		self.calculateAccumulatedFrame().height
	}
	var endOfGroup: CGFloat = 0
	var state: State = .walking
	let numberOfPeople: Int
	var line: CustomerLine = CustomerLine.A
	var sadBar: SKSpriteNode?
	var gameController: GameController!

	var chars = [(SKSpriteNode, [SKTexture], [SKTexture])]()

	init(numberOfPeople: Int) {
		self.numberOfPeople = numberOfPeople
		super.init()
		//generating array of random numbers from 1 to 5. That will be char sprites.
		var persons = [Int]()
		var random = Int.random(in: 1...5)
		for _ in 1...numberOfPeople {
			while persons.contains(random) {
				random = Int.random(in: 1...5)
			}
			persons.append(random)
		}
		
		for (index, person) in persons.enumerated() {
			var textures = [SKTexture]()
			var awayTextures = [SKTexture]()
			let personWalkAtlas = SKTextureAtlas(named: "Char\(person)walking")
			let personLeaveAtlas = SKTextureAtlas(named: "Char\(person)leaving")
			for n in 0...7 {
				textures.append(personWalkAtlas.textureNamed("Walk\(n)"))
				textures[n].filteringMode = .nearest
				awayTextures.append(personLeaveAtlas.textureNamed("Walk\(n)"))
				awayTextures[n].filteringMode = .nearest
			}
			let char = SKSpriteNode(texture: textures[0])
			char.position.y = CGFloat(index * 30)
			char.zPosition = CGFloat(10 - index)
			chars.append((char, textures, awayTextures))
			self.addChild(char)
		}
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func walk(downTo endPosition: CGFloat) {
		state = .walking
		//here we find the point of end of the customer group. Next customer will go to this point.
		//+10 is some space to separate different groups.
		endOfGroup = endPosition + groupHeight + 10
		
		let walkingTime = (self.position.y - endPosition) / gameController.customerSpeed
		for (char, textures, _) in chars {
			let animate = SKAction.repeatForever(SKAction.animate(with: textures, timePerFrame: Constants.customerAnimationRate, resize: false, restore: true))
			char.run(animate)
		}
		let move = SKAction.moveTo(y: endPosition, duration: TimeInterval(walkingTime))
		let stopActions = SKAction.run { [unowned self] in
			self.removeAllActions()
			for child in self.children {
				child.removeAllActions()
			}
		}
		let moveAndStop = SKAction.sequence([move, stopActions])
		let walk = SKAction.group([moveAndStop])
		let wait = SKAction.run { [unowned self] in
			if self.state != .waitingForTheFood {
				self.state = .waitingInTheLine
			}
			if endPosition == Constants.endPosition.y {
				self.startGettingSad()
				self.gameController.takeOrder(from: self)
			}
		}
		let myPeopleGo = SKAction.sequence([walk, wait])
		self.run(myPeopleGo, withKey: "walk")
	}
	
	func startGettingSad () {
		
		state = .waitingForTheFood
		var sadBarTextures = [SKTexture]()
		
		for n in 0...6 {
			sadBarTextures.append(SKTexture(imageNamed: "HappinessBar\(n)"))
		}
		
		sadBar = SKSpriteNode(texture: sadBarTextures[0], size: CGSize(width: 32, height: 14) )
		sadBar?.zPosition = 11
		sadBar?.position = CGPoint(x: 0, y: 20)
		let sadAction = SKAction.animate(with: sadBarTextures, timePerFrame: Constants.sadTime / 7)
		let killAction = SKAction.run {  [unowned self] in 
			self.gameController.gameOver()
		}
		sadBar?.run(SKAction.sequence([sadAction, killAction]))
		self.addChild(sadBar!)
	}
	
	func getKebabs() {
		//stop being sad and remove from the game
		sadBar?.removeAllActions()
		sadBar?.removeFromParent()
		if state == .leaving {
			return
		}
		state = .leaving
		let scene = self.scene as! GameScene
		scene.removeCustomer(at: line)
		scene.moveCustomers(in: self.line)
		gameController.giveKebab(to: self)		
	}
	
	func personWalksAway() {
		var whereToGo: CGFloat
		let walkingTime = (Constants.StartPostition.lineA.x + 250) / gameController.customerSpeed
		let charRotation: CGFloat
		switch line {
			case .A:
				whereToGo = -250
				charRotation = -CGFloat(Double.pi / 2)
			case .B:
				whereToGo = 250
				charRotation = CGFloat(Double.pi / 2)
		}
		let (firstChar, _, awayTextures) = chars.first!
		let rotate = SKAction.rotate(toAngle: charRotation, duration: 0)
		let animateWithTray = SKAction.repeatForever(SKAction.animate(with: awayTextures, timePerFrame: Constants.customerAnimationRate))
		let moveAway = SKAction.moveTo(x: whereToGo, duration: TimeInterval(walkingTime))
		let walkAway = SKAction.group([animateWithTray, moveAway])
		firstChar.run(SKAction.sequence([rotate, walkAway]))
		
		chars.removeFirst()

		if chars.isEmpty {
			self.run(.sequence([
				.wait(forDuration: TimeInterval(walkingTime)),
				.run {
					self.removeFromParent()
				}
			]))
			
		}
		
		
		for (char, textures, _) in chars {
			char.removeAllActions()
			let animateEmptyHands = SKAction.repeatForever(SKAction.animate(with: textures, timePerFrame: Constants.customerAnimationRate, resize: false, restore: true))
			char.run(animateEmptyHands)
			let moveToTruck = SKAction.moveBy(x: 0, y: -30, duration: TimeInterval(30 / gameController.customerSpeed))
			let stop = SKAction.run(char.removeAllActions)
			var knock: SKAction
			if char == chars.first?.0 {
				knock = SKAction.run {
					self.gameController.giveKebab(to: self)
				}
			} else {
				knock = SKAction()
			}
			let walkToTruck = SKAction.sequence([moveToTruck, stop, knock])
			char.run(walkToTruck)
		}
	}
}
