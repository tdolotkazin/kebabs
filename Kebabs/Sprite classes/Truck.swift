import SpriteKit

protocol TruckDelegate: AnyObject {
	func didFinishCooking(kebabs: Int)
}

class Truck: SKSpriteNode, ButtonDelegate {
	
	weak var delegate: TruckDelegate?
	var gameController: GameController!
	var audioPlayer: AudioPlayer!
	let animatedNodes = SKNode()
	var buttonsAreDisabled = false {
		didSet {
			for button in buttons {
				button.isEnabled = !buttonsAreDisabled
			}
		}
	}
	private var kebabsAreCooking: Int = 0
	private var kebabLabel: SKLabelNode?
	private var smokeNode: SKEmitterNode?
	private var buttons = [Button]()
	private var grill: SKSpriteNode?
	private var chef: Chef?
	private var cookingBar: SKSpriteNode?
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		addChild(animatedNodes)
		self.isPaused = false
		for child in children {
			if let button = child as? Button {
				button.texture?.filteringMode = .nearest
				buttons.append(button)
				button.delegate = self
				button.zPosition = 2
				button.position.y -= 200
			}
		}
		createKebabLabel()
		createGrill()
		addChef()
	}
	
	//MARK: - Initialization of sprites
	
	private func createKebabLabel() {
		kebabLabel = SKLabelNode()
		kebabLabel?.makePixelFont()
		kebabLabel?.fontSize = 20
		kebabLabel?.position = CGPoint(x: 15, y: -23)
		kebabLabel?.zPosition = 10
		animatedNodes.addChild(kebabLabel!)
	}
	
	private func createGrill() {
		guard let grill = (childNode(withName: "Grill") as? SKSpriteNode) else {
			fatalError("No grill node!")
		}
		grill.move(toParent: animatedNodes)
		grill.zPosition = 1
		var grillTextures = [SKTexture]()
		let grillAtlas = SKTextureAtlas(named: "Grill")
		for n in 0...3 {
			grillTextures.append(grillAtlas.textureNamed("Grill\(n)"))
			grillTextures[n].filteringMode = .nearest
		}
		let blink = SKAction.animate(with: grillTextures, timePerFrame: 0.1, resize: false, restore: true)
		let wait = SKAction.wait(forDuration: 3)
		let startBlinking = SKAction.sequence([blink, wait])
		grill.run(SKAction.repeatForever(startBlinking))
	}
	
	private func addChef() {
		chef = Chef()
		chef?.position = Constants.chefPostition
		animatedNodes.addChild(chef!)
	}
}

//MARK: - Buttons methods

extension Truck {
	
	func showButtons() {
		for button in buttons {
			button.run(SKAction.moveBy(x: 0, y: 200, duration: Constants.UIbuildTime))
			
		}
	}
	
	func hideButtons() {
		for button in buttons {
			button.run(SKAction.moveBy(x: 0, y: -200, duration: Constants.UIbuildTime))
		}
	}
	
	func buttonPressed(button: Button) {
		guard let kebabsToCook = Int(button.name!) else {
			fatalError("Unknown button in Truck")
		}
		startCooking(numberOfKebabs: kebabsToCook)
	}
	
}

//MARK: - Cooking methods

extension Truck {

	private func startCooking(numberOfKebabs: Int) {
		buttonsAreDisabled = true
		kebabsAreCooking = numberOfKebabs
		run(.sequence([
			.run(startSmoke),
			.run { self.chef?.cook(for: Constants.chefAnimationTime) },
			.wait(forDuration: Constants.chefAnimationTime),
			.run(stopSmoke)
		]))
		audioPlayer.playEffect(of: .Cooking)
		delegate?.didFinishCooking(kebabs: numberOfKebabs)
		updateLabels()
	}
	
	func updateLabels() {
		let label = gameController.kebabs != 0 ? String(gameController.kebabs) : ""
		kebabLabel?.text = label
	}
}

extension Truck {
	func restart() {
		cookingBar?.removeAllActions()
		cookingBar?.removeFromParent()
		chef?.removeAllActions()
		chef?.run((chef?.moveTo(Constants.chefPostition, time: 0))!)
		animatedNodes.isPaused = false
		showButtons()
		kebabsAreCooking = 0
		stopSmoke()
		updateLabels()
		buttonsAreDisabled = false
	}
}

//MARK: - Smoke section

extension Truck {
	func startSmoke() {
		if let smokeNode = smokeNode {
			smokeNode.particleBirthRate = 20
		} else {
			smokeNode = SKEmitterNode(fileNamed: "Smoke")
			smokeNode?.position = CGPoint(x: 20, y: -25)
			smokeNode?.zPosition = 1
			animatedNodes.addChild(smokeNode!)
		}
	}
	
	func stopSmoke() {
		smokeNode?.particleBirthRate = 0
	}
}
