import UIKit
import SpriteKit


class GameViewController: UIViewController, ShareDelegate {
	
    override func viewDidLoad() {
        super.viewDidLoad()

		
		if let scene = GameScene(fileNamed: "GameScene") {
			scene.scaleMode = .aspectFill
			scene.scoreBoard?.delegate = self
			
                // Present the scene
                if let view = self.view as! SKView? {
                    view.presentScene(scene)
                    view.ignoresSiblingOrder = true
                }
            }
	}
	
    override var shouldAutorotate: Bool {
        return false
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
	
	func showShareView(shareImage: UIImage, shareText: String) {
		let items = [shareText, shareImage] as [Any]
		let shareVC = UIActivityViewController(activityItems: items, applicationActivities: nil)
		present(shareVC, animated: true, completion: nil)
		if let popOver = shareVC.popoverPresentationController {
			popOver.sourceView = self.view
			popOver.sourceRect = CGRect(x: view.frame.midX, y: view.frame.midY, width: 0, height: 0)
			popOver.permittedArrowDirections = []
		}
	}
}
