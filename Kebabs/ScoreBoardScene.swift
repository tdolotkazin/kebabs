import SpriteKit

class ScoreBoard: SKScene, ButtonDelegate {
	
	override func sceneDidLoad() {
		print("scoreboard did load")
		for child in children {
			if let button = child as? Button {
				button.delegate = self
				button.isUserInteractionEnabled = true
			}
		}
	}
	
	func buttonPressed(button: Button) {
		self.view?.presentScene(SKScene(fileNamed: "GameScene")!, transition: .crossFade(withDuration: 1))
	}
	
}
