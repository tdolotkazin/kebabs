import SpriteKit

class GameScene: SKScene, GameDelegate, ButtonDelegate {
	
	let gameController = GameController()
	var truck: Truck!
	var lineA = [Customer]()
	var lineB = [Customer]()
	var currentLineIsA = true
	var customers = SKNode()
	var scoreLabel: SKLabelNode!
	var scoreBoard: ScoreBoard?
	var audioPlayer: AudioPlayer!
	var tutorial: Tutorial!
	var pauseButton: Button!
	var settingsButtons: SettingsButtons!
	var timer: SKNode!
	var realPaused = false
	var comesFromBackground = false
	
	override func sceneDidLoad() {
		super.sceneDidLoad()
		let appDelegate = UIApplication.shared
		NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: appDelegate)
		audioPlayer = AudioPlayer()
		audioPlayer.gameController = gameController
		gameController.audioPlayer = audioPlayer
		addChild(audioPlayer)
		audioPlayer.playMusic()
		tutorial = Tutorial()
		addChild(tutorial)
//		tutorial.position.y = 20
		truck = childNode(withName: "Truck") as? Truck
		truck?.gameController = gameController
		truck.delegate = gameController
		truck.audioPlayer = audioPlayer
		gameController.truck = truck
		scoreLabel = childNode(withName: "ScoreLabel") as? SKLabelNode
		scoreLabel.isHidden = true
		gameController.delegate = self
		scoreBoard = (childNode(withName: "//scoreBoard") as! ScoreBoard)
		scoreBoard?.gameController = gameController
		addChild(customers)
		settingsButtons = SettingsButtons()
		settingsButtons.audioPlayer = audioPlayer
		settingsButtons.tutorial = tutorial
		settingsButtons.createButtons()
		settingsButtons.zPosition = 3
		addChild(settingsButtons)
		let mainMenu = MainMenu(scene: self)
		addChild(mainMenu)
		let pauseButtonTexture = SKTexture(imageNamed: "Pause")
		pauseButtonTexture.filteringMode = .nearest
		pauseButton = Button(texture: pauseButtonTexture)
		pauseButton.onTexture = pauseButtonTexture
		pauseButton.offTexture = SKTexture(imageNamed: "Play")
		pauseButton.offTexture?.filteringMode = .nearest
		pauseButton.position = CGPoint(x: 0, y: 350)
		addChild(pauseButton)
		pauseButton.isHidden = true
		pauseButton.delegate = self
		pauseButton.stateIsOn = true
		timer = SKNode()
		addChild(timer)
	}
	
	//MARK: - Customers methods
	
	func createNewCustomer(numberOfPeopleInGroup: Int) {
		var start: CGPoint
		var walkTo: CGFloat
		let customer = Customer(numberOfPeople: numberOfPeopleInGroup)
		var line: CustomerLine
		if currentLineIsA {
			line = .A
		} else {
			line = .B
		}
		currentLineIsA.toggle()
		customer.name = "customer"
		customer.gameController = gameController
		switch line {
			case .A:
				customer.line = .A
				start = Constants.StartPostition.lineA
				lineA.append(customer)
				if lineA.count != 1 {
					walkTo = lineA[lineA.count - 2].endOfGroup
				} else {
					walkTo = Constants.endPosition.y
			}
			
			case .B:
				customer.line = .B
				start = Constants.StartPostition.lineB
				lineB.append(customer)
				if lineB.count != 1 {
					walkTo = lineB[lineB.count - 2].endOfGroup
				} else {
					walkTo = Constants.endPosition.y
			}
			
		}
		customer.position = start
		customer.walk(downTo: walkTo)
		customers.addChild(customer)
	}
	
	func randomPeopleInGroup(probabilities: [Double]) -> Int {
		let sum = probabilities.reduce(0, +)
		let random = Double.random(in: 0...sum)
		var sumOfProbabilities = 0.0
		for (index, range) in probabilities.enumerated() {
			sumOfProbabilities += range
			if random < sumOfProbabilities {
				return index + 1
			}
		}
		return probabilities.count
	}
	
	func startCreatingRandomCustomers() {
		//if starting new level - remove old spawning action and wait.
		if customers.action(forKey: "spawning") != nil {
			customers.removeAction(forKey: "spawning")
			customers.run(.sequence([
				.wait(forDuration: gameController.customerSpawnTime),
				.run(startCreatingRandomCustomers)
			]))
			return
				
		}
		customers.run(SKAction.repeatForever(
			SKAction.sequence([
				SKAction.run { [unowned self] in
					self.createNewCustomer(numberOfPeopleInGroup: self.randomPeopleInGroup(probabilities: [0.5, 0.3, 0.2, 0.1, 0.05]))
				},
				SKAction.wait(forDuration: gameController.customerSpawnTime)
			])), withKey: "spawning")
	}
	
	func moveCustomers(in line: CustomerLine) {
		var lineX: [Customer]
		switch line {
			case .A:
				lineX = lineA
			case .B:
				lineX = lineB
		}
		for (index, customer) in lineX.enumerated() {
			if customer.state != .waitingForTheFood {
				if index != 0 {
					customer.walk(downTo: lineX[index - 1].endOfGroup)
				} else {
					customer.walk(downTo: Constants.endPosition.y)
				}
			}
		}
	}
	
	func moveAllCustomers() {
		moveCustomers(in: .A)
		moveCustomers(in: .B)
	}
	
	func removeCustomer(at line: CustomerLine) {
		switch line {
			case .A:
				lineA.removeFirst()
			case .B:
				lineB.removeFirst()
		}
	}
	
	//MARK: - Pause methods
	
	func buttonPressed(button: Button) {
		audioPlayer.playEffect(of: .ButtonPressed)
		if !realPaused {
			pauseGame()
		} else {
			unpauseGame()
		}
	}
	
	@objc func didBecomeActive() {
		print("Did become active")
		comesFromBackground = true
		
	}
	
	func pauseGame() {
		if !realPaused {
			truck.hideButtons()
			settingsButtons.showAll(during: gameController.state)
		}
		gameController.state = .Pause
		realPaused = true
		customers.isPaused = true
		truck.animatedNodes.isPaused = true
		pauseButton.stateIsOn? = false
		pauseButton.updateState()
		timer.isPaused = true
	}
	
	func unpauseGame() {
		if realPaused {
			truck.showButtons()
			settingsButtons.hideAll()
		}
		gameController.state = .Play
		realPaused = false
		customers.isPaused = false
		truck.animatedNodes.isPaused = false
		pauseButton.stateIsOn? = true
		pauseButton.updateState()
		timer.isPaused = false
	}
	
	//MARK: - Update methods
	
	override func update(_ currentTime: TimeInterval) {
		updateScores()
		if comesFromBackground {
			if !audioPlayer.musicIsEnabled || gameController.state == .GameOver {
				audioPlayer.pauseMusic()
			}
			comesFromBackground = false
			switch gameController.state {
				case .Play, .Pause:
				pauseGame()
				case .GameOver:
					customers.isPaused = true
//					truck.isPaused = true
				default: break
			}
		}
	}
	
	func updateScores() {
		scoreLabel?.text = String(gameController.score)
	}
	
	//MARK: - Game Over and Restart
	
	func gameOver(score: Int) {
		customers.isPaused = true
		audioPlayer.pauseMusic()
		audioPlayer.playEffect(of: .GameOver)
		truck.hideButtons()
		truck.animatedNodes.isPaused = true
		pauseButton.isHidden = true
		customers.removeAction(forKey: "spawning")
		scoreBoard?.gameOver()
	}
	
	func restart() {
		if realPaused {
			unpauseGame()
		}
		customers.isPaused = false
		pauseButton.isHidden = false
		scoreLabel.isHidden = false
		gameController.kebabs = 0
		truck.restart()
		gameController.restartGame()
		currentLineIsA = true
		lineA.removeAll()
		lineB.removeAll()
		customers.removeAllChildren()
		updateScores()
		startCreatingRandomCustomers()
		audioPlayer.playMusic()
		runTimer()
	}
	
	//MARK: - Timer
	
	func runTimer() {
		timer.removeAllActions()
		timer.run(SKAction.repeatForever(
			(SKAction.sequence([
				SKAction.wait(forDuration: Constants.timeToNextLevel),
				SKAction.run {
					self.gameController.nextLevel()
					self.startCreatingRandomCustomers()
					self.moveAllCustomers()
				}]))))
	}
	
}
