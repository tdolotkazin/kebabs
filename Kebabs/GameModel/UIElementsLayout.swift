import SpriteKit

struct Layout {
	
	struct SettingsButtons {
		static let audioButton = CGPoint(x: -100, y: -323)
		static let musicButton = CGPoint(x: 0, y: -323)
		static let tutorialButton = CGPoint(x: 100, y: -323)
	}
	
	struct MainMenu {
		static let logo = CGPoint(x: 0, y: 249)
		static let playButton = CGPoint(x: 0, y: -60)
		static let bestScore = CGPoint(x: 0, y: 90)
		static let bestScoreNumber = CGPoint(x: 0, y: 60)
		static let audioButton = CGPoint(x: -100, y: -323)
		static let musicButton = CGPoint(x: 0, y: -323)
		static let tutorialButton = CGPoint(x: 100, y: -323)
	}
	
	
}
