import UIKit

protocol GameDelegate: AnyObject {
	func gameOver(score: Int)
	
}

class GameController: TruckDelegate {

	var truck: Truck!
	weak var delegate: GameDelegate?
	var score: Int = 0
	var sequence: Int = 0
	var customerSpeed = Constants.customerSpeed
	var customerSpawnTime = Constants.customerSpawnTime
	var bestScore:Int {
		let defaults = UserDefaults.standard
		let bestScore = defaults.integer(forKey: "bestScore")
		if bestScore > score {
			return bestScore
		} else {
			defaults.set(score, forKey: "bestScore")
			return score
		}
	}
	var kebabs: Int = 0 {
		didSet {
			if kebabs == 0 {
				truck.buttonsAreDisabled = false
				isServing = false
				sequence = 0
			}
		}
	}
	
	var state = GameState.MainMenu
	private var servingLine = [(customer: Customer, cost: Int)]()
	private var willBeGameOver = false
	var audioPlayer: AudioPlayer?
	var isServing = false
	
	func restartGame() {
		customerSpawnTime = Constants.customerSpawnTime
		customerSpeed = Constants.customerSpeed
		score = 0
		kebabs = 0
		state = .Play
		sequence = 0
		isServing = false
		servingLine.removeAll()
	}
	
	func nextLevel() {
		customerSpeed *= CGFloat(1 + Constants.speedRate)
		customerSpawnTime *= Double(1 - Constants.spawnRate)
	}
	
	func takeOrder(from customer: Customer) {
		servingLine.append((customer: customer, cost: 0))
		if isServing {
			feedAll()
		}
	}
	
	func didFinishCooking(kebabs: Int) {
		self.kebabs = kebabs
		isServing = true
		feedAll()
	}
	

	func feedAll() {
		var totalPeopleToFed = 0
		for (customer, _) in servingLine {
			totalPeopleToFed += customer.chars.count
		}

		guard let firstCustomerCount = servingLine.first?.customer.chars.count else {
			return
		}
	
		switch kebabs {
			case 0..<firstCustomerCount:
				gameOver()
			case firstCustomerCount:
				servingLine.first?.customer.getKebabs()
			case firstCustomerCount..<totalPeopleToFed:
				willBeGameOver = true
				servingLine.first?.customer.getKebabs()
			default:
				for item in servingLine {
					item.customer.getKebabs()
			}
		}
	}
	
	func giveKebab(to customer: Customer) {
		
		let index = servingLine.firstIndex { existing -> Bool in
			existing.customer == customer
		}
		
		//Check if it's the first char of the customer group.
		if customer.chars.count == customer.numberOfPeople {
			servingLine[index!].cost = sequence
			sequence += 1
			if customer.numberOfPeople > kebabs {
				gameOver()
				return
			}
		}
		kebabs -= 1
		truck.updateLabels()
		score += Int(pow(Double(10), Double(servingLine[index!].cost)))
		audioPlayer?.playCoinEffect(n: servingLine[index!].cost + 1)
		
		//Check if it's the last char in customer group.
		if customer.chars.count == 1 {
			servingLine.removeAll { (oldCustomer) -> Bool in
				oldCustomer.customer == customer
			}
			
			
			if willBeGameOver {
				willBeGameOver = false
				gameOver()
			}
		}
		customer.personWalksAway()
	}
	
	func gameOver() {

		//This is very ugly but I've no idea why Share Button calls this method for a second time.
		if state != .GameOver {
			state = .GameOver
			delegate?.gameOver(score: score)
		}
	}
}
