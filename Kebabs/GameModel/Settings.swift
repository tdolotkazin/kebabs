import SpriteKit

struct Constants {
	static let customerSpeed: CGFloat = 110
	static let sadTime: TimeInterval = 4
	static let cookingTime: TimeInterval = 0
	static let customerSpawnTime: TimeInterval = 1.75
	static let timeToNextLevel: TimeInterval = 30
	static let speedRate: CGFloat = 0.1
	static let spawnRate: CGFloat = 0.165
	static let chefAnimationTime: TimeInterval = 0.3
	static let customerAnimationRate: TimeInterval = 0.05
	static var bestScore: Int = 0
	static let chefPostition = CGPoint(x: -4, y: 26)
	static let cookingPosition = CGPoint(x: -54, y: 2)
	static let grillPosition = CGPoint(x: 18, y: 3)
	static let UIbuildTime = 0.5
	
	struct StartPostition {
		static let lineA = CGPoint(x: -64, y: 450)
		static let lineB = CGPoint(x: 64, y: 450)
	}
	
	static let endPosition = CGPoint(x: 0, y: -127)
	
}

enum CustomerLine: CaseIterable {
	case A
	case B
}

enum MusicButtonTexture: String {
	mutating func toggle() {
		switch self {
			case .on:
				self = .off
			case .off:
				self = .on
		}
	}
	case on = "Music on"
	case off = "Music off"
}

enum AudioButtonTexture: String {
	case on = "Audio on"
	case off = "Audio off"
	mutating func toggle() {
		switch self {
			case .on:
				self = .off
			case .off:
				self = .on
		}
	}
}

enum GameState {
	case MainMenu
	case Play
	case Pause
	case GameOver
}

enum SoundEffects:String {
	case Cooking = "Cooking the kebab"
	case ButtonPressed = "ui"
	case GameOver = "defeat"
	case Coin = "coins "
}
