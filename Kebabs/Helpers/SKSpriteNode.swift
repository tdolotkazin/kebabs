import SpriteKit

extension SKSpriteNode {
	func createShadow() {
		let shadow = SKSpriteNode(texture: self.texture)
		shadow.blendMode = .alpha
		shadow.colorBlendFactor = 1
		shadow.color = .black
		shadow.alpha = 0.4
		shadow.position = CGPoint(x: -3, y: -3)
		shadow.zPosition = -1
		self.addChild(shadow)
	}
}
