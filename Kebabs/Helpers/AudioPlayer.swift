import SpriteKit

class AudioPlayer: SKNode {
	
	var musicIsEnabled: Bool = true {
		didSet {
			if musicIsEnabled {
				resumeMusic()
			} else {
				pauseMusic()
			}
			saveSettings()
		}
	}
	var effectsAreEnabled: Bool = true {
		didSet {
			saveSettings()
		}
	}
	private var music: SKAudioNode?
	private var effect: SKAudioNode?
	var gameController: GameController!
	
	override init() {
		super.init()
		let defaults = UserDefaults.standard
		musicIsEnabled = defaults.bool(forKey: "music")
		effectsAreEnabled = defaults.bool(forKey: "effects")
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func saveSettings(){
		let defaults = UserDefaults.standard
		defaults.set(musicIsEnabled, forKey: "music")
		defaults.set(effectsAreEnabled, forKey: "effects")
	}
	
	func playMusic() {
		music?.removeAllActions()
		music?.removeFromParent()
		music = nil
		
		if musicIsEnabled {
//			if music != nil {
//				music?.removeFromParent()
//			}
			var fileName: String
			var isLooped: Bool
			switch gameController.state {
				case .MainMenu:
					fileName = "Intro"
					isLooped = false
				default:
					fileName = "Game music"
					isLooped = true
			}
			if let musicURL = Bundle.main.url(forResource: fileName, withExtension: ".mp3") {
				music = SKAudioNode(url: musicURL)
				music?.autoplayLooped = isLooped
				addChild(music!)
				music?.run(SKAction.play())
			}
		}
//		else {
//			music = nil
//		}
	}
	
	func resumeMusic() {
		if let music = music {
			music.run(SKAction.changeVolume(to: 1, duration: 0.5))
		} else {
			playMusic()
		}
	}
	
	func pauseMusic() {
		if let music = music {
			music.run(SKAction.changeVolume(to: 0, duration: 0.5))
		}
	}
	
	func playEffect(of name: SoundEffects) {
		if effectsAreEnabled {
			if effect != nil {
				effect?.removeFromParent()
			}
			if let effectURL = Bundle.main.url(forResource: name.rawValue, withExtension: ".mp3") {
				effect = SKAudioNode(url: effectURL)
				effect!.autoplayLooped = false
				addChild(effect!)
				effect!.run(SKAction.play())
			} else {
				fatalError("No effect file found!")
			}
		}
	}
	
	func playCoinEffect(n: Int) {
		if effectsAreEnabled {
			if effect != nil {
				effect?.removeFromParent()
			}
			if let effectURL = Bundle.main.url(forResource: "coins \(n)", withExtension: ".mp3") {
				effect = SKAudioNode(url: effectURL)
				effect!.autoplayLooped = false
				addChild(effect!)
				effect!.run(SKAction.play())
			} else {
				fatalError("No coin effect file found")
			}
		}
	}
}
