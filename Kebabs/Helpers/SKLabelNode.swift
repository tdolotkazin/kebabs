import SpriteKit

extension SKLabelNode {
	func makePixelFont() {
		fontName = "Press Start 2P"
		fontColor = .white
		verticalAlignmentMode = .center
	}
}
