import XCTest
@testable import Kebabs

class KebabsTests: XCTestCase {

	var sut: GameController!

	
    override func setUpWithError() throws {
		try super.setUpWithError()
		sut = GameController()
    }

    override func tearDownWithError() throws {
		sut = nil
		try super.tearDownWithError()
		
    }

	
	func testScoreComputing() {
		
		let customer1 = Customer(numberOfPeople: 5)
		let customer2 = Customer(numberOfPeople: 2)
		sut.takeOrder(from: customer1)
		sut.takeOrder(from: customer2)
		sut.kebabs = 7
		sut.feedAll()
		
		XCTAssertEqual(sut.score, 25, "Score is wrong")
	}
}
